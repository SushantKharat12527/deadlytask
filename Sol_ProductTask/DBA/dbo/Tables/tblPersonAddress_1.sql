﻿CREATE TABLE [dbo].[tblPersonAddress] (
    [Person_Id] INT            NOT NULL,
    [City]      NVARCHAR (MAX) NOT NULL,
    [State]     NVARCHAR (MAX) NOT NULL,
    [Pincode]   NVARCHAR (MAX) NOT NULL,
    [Address]   NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [FK__tblPerson__Perso__182C9B23] FOREIGN KEY ([Person_Id]) REFERENCES [dbo].[tblPerson] ([Person_Id])
);

