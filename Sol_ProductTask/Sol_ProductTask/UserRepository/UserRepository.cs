﻿using Sol_ProductTask.Entity;
using Sol_ProductTask.Entity.PersonEntity;
using Sol_ProductTask.ORD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Sol_ProductTask.UserRepository
{
    public class UserPanelRepository
    {
        #region Declaration
        private PersonDataContext Dc = null;
        #endregion

        #region Constructor
        public UserPanelRepository()
        {
            Dc = new PersonDataContext();
        }
        #endregion

        public async Task<bool> Insert(PersonEntity personEntityObj)
        {
            int? status = null;
            string message = null;

            return await Task.Run(() =>
            {

                var setQuery =
                    Dc
                    ?.uspSetUser(
                        "Insert"
                        , personEntityObj?.PersonId
                        , personEntityObj?.FirstName
                        , personEntityObj?.LastName
                        , personEntityObj?.UserType
                        , personEntityObj?.LoginEntity?.UserName
                        , personEntityObj?.LoginEntity?.Password
                        , personEntityObj?.CommunicationEntity?.MobileNo
                        , personEntityObj?.CommunicationEntity?.EmailId
                        , ref status
                        , ref message
                    );

                return (status == 1) ? true : false;

            });
        }

        public async Task<bool> Update(PersonEntity entityObj)
        {
            int? status = null;
            String message = null;

            return await Task.Run(() =>
            {

                var setQuery =
                    Dc
                    ?.uspSetUser(
                        "Update"
                        , entityObj?.PersonId
                        , entityObj?.FirstName
                        , entityObj?.LastName
                        , entityObj?.UserType
                        , entityObj?.LoginEntity?.UserName
                        , entityObj?.LoginEntity?.Password
                        , entityObj?.CommunicationEntity?.MobileNo
                        , entityObj?.CommunicationEntity?.EmailId
                        , ref status
                        , ref message
                    );

                return (status == 1) ? true : false;
            });

        }

        public async Task<bool> Delete(PersonEntity entityObj)
        {
            int? status = null;
            String message = null;

            return await Task.Run(() =>
            {

                var setQuery =
                    Dc
                    ?.uspSetUser(
                        "Delete"
                        , entityObj?.PersonId
                        , entityObj?.FirstName
                        , entityObj?.LastName
                        , entityObj?.UserType
                        , entityObj?.LoginEntity?.UserName
                        , entityObj?.LoginEntity?.Password
                        , entityObj?.CommunicationEntity?.MobileNo
                        , entityObj?.CommunicationEntity?.EmailId
                        , ref status
                        , ref message
                    );

                return (status == 1) ? true : false;
            });
        }

        }

    }


