﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_ProductTask.Entity.SalesEntity
{
   public class SalesTransactionEntity
    {
        public decimal? TransactionId { get; set; }

        public DateTime SalesDate { get; set; }

        public decimal ProductId { get; set; }

        public int OrderQuantity { get; set; }

        public decimal? TotalAmount { get; set; }

        public string Status { get; set; }

        public decimal PersonId { get; set; }
    }
}
