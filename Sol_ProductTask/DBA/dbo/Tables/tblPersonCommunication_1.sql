﻿CREATE TABLE [dbo].[tblPersonCommunication] (
    [Person_Id] INT            NOT NULL,
    [Mobile_No] NVARCHAR (MAX) NOT NULL,
    [EmailId]   NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [FK__tblPerson__Perso__15502E78] FOREIGN KEY ([Person_Id]) REFERENCES [dbo].[tblPerson] ([Person_Id])
);

