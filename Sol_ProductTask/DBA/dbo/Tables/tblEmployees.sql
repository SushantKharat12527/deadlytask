﻿CREATE TABLE [dbo].[tblEmployees] (
    [Person_Id]   INT NOT NULL,
    [Employee_Id] INT IDENTITY (1, 1) NOT NULL,
    FOREIGN KEY ([Person_Id]) REFERENCES [dbo].[tblPerson] ([Person_Id])
);

