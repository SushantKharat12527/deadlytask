﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_ProductTask.Entity.PersonEntity
{
   public class PersonCommunicationEntity
    {
        public string MobileNo { get; set; }

        public string EmailId { get; set; }
    }
}
