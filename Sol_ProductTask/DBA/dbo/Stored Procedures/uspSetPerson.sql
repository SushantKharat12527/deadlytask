﻿CREATE PROCEDURE [dbo].[uspSetUser]
	
	@Command Varchar(50)=NULL,
	
	@PersonId Numeric(18,0)=NULL,

	@FirstName Varchar(50)=NULL,
	@LastName Varchar(50)=NULL,
	@PersonType varchar(50)=null,

	@UserName Varchar(50)=NULL,
	@Password Varchar(50)=NULL,

	@MobileNo Varchar(50)=NULL,
	@EmailId Varchar(50)=NULL,

	@Status int=NULL OUT,
	@Message Varchar(MAX)=NULL OUT

	 

AS
	BEGIN 
		
		DECLARE @ErrorMessage varchar(MAX)

			IF @Command='Insert'
				BEGIN
					
					BEGIN TRANSACTION

					BEGIN TRY 
						
						INSERT INTO tblPerson
						(
							FirstName,
							LastName,
							Person_Type
						)
						VALUES
						(
							@FirstName,
							@LastName,
							@PersonType
						)

						SET @PersonId=@@IDENTITY
							
							-- Call User Login Stored Procedures
							EXEC uspSetPersonLogin @Command,@PersonId,@UserName,@Password

							-- Call User Communication Stored Procedure
							EXEC uspSetPersonCommunication @Command,@PersonId,@MobileNo,@EmailId


						SET @Status=1
						SET @Message='Insert Succesfully'

						COMMIT TRANSACTION
					END TRY 

					BEGIN CATCH 
						SET @ErrorMessage=ERROR_MESSAGE()
						ROLLBACK TRANSACTION
						
						SET @Status=0
						SET @Message='Insert Exception'

						RAISERROR(@ErrorMessage,16,1)
					END CATCH

				END
			ELSE IF @Command='Update'
				BEGIN 
						
					BEGIN TRANSACTION

					BEGIN TRY 

						SELECT 
						@FirstName=CASE WHEN @FirstName IS NULL THEN U.FirstName ELSE @FirstName END,
						@LastName=CASE WHEN @LastName IS NULL THEN U.LastName ELSE @LastName END
						FROM tblPerson AS U
							WHERE U.Person_Id=@PersonId

							UPDATE tblPerson
								SET 
									FirstName=@FirstName,
									LastName=@LastName,
									Person_Type=@PersonType
										WHERE Person_Id=@PersonId

								EXEC uspSetUserLogin @Command,@PersonId,@UserName,@Password

								EXEC uspSetUserCommunication @Command,@PersonId,@MobileNo,@EmailId

								SET @Status=1
								SET @Message='Update Succesfully'

								COMMIT TRANSACTION

					END TRY 

					BEGIN CATCH 
						SET @ErrorMessage=ERROR_MESSAGE()
						ROLLBACK TRANSACTION
						
						SET @Status=0
						SET @Message='Update Exception'

						RAISERROR(@ErrorMessage,16,1)
					END CATCH


					
				END
			ELSE IF @Command='Delete'
				BEGIN
					BEGIN TRANSACTION

					BEGIN TRY 
						
						DELETE FROM tblPerson
							WHERE Person_Id=@PersonId

							EXEC uspSetUserLogin @Command,@PersonId,@UserName,@Password

								EXEC uspSetUserCommunication @Command,@PersonId,@MobileNo,@EmailId

								SET @Status=1
								SET @Message='Delete Succesfully'

										COMMIT TRANSACTION

					END TRY 

					BEGIN CATCH 
					
						SET @ErrorMessage=ERROR_MESSAGE()
						
						ROLLBACK TRANSACTION
						
						SET @Status=0
						SET @Message='Delete Exception'

						RAISERROR(@ErrorMessage,16,1)
						
						

					END CATCH 

					
				END

	END 
GO
