﻿using Sol_ProductTask.Entity;
using Sol_ProductTask.ORD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_ProductTask.Admin
{
    public class AdminRepository
    {
        #region Declaration
        public AdminDataContext dc = null;
       public tblProduct tblProductObj = null;
        #endregion

        #region Constructor
        public AdminRepository()
        {
            dc = new AdminDataContext();
        }
        #endregion

        #region Insert

        #region Public Method
        public string InsertProduct(ProductEntity productEntityObj)
        {
            int Identity  = this.AddProductInfo(productEntityObj);
            return "Inserted Successfully";
        }
        #endregion

        #region Private Method
        private int AddProductInfo(ProductEntity productEntityObj)
        {
            var tblProductObj = new tblProduct()
            {
                Product_Id = productEntityObj.Product_Id,
                Product_Name = productEntityObj.Product_Name,
                Product_Price = productEntityObj.Product_Price,
                PRoduct_AvailableQuantity = productEntityObj.PRoduct_AvailableQuantity
            };

            dc.
            tblProducts
            .InsertOnSubmit(tblProductObj);

            dc.SubmitChanges();

            var getLastIdentityValue = productEntityObj.Product_Id;
            return Convert.ToInt32(getLastIdentityValue);

        }
        #endregion
        #endregion

        #region Update

        #region Public Method
        public String UpdateProductTable(ProductEntity productEntityObj)
        {
           // tblProductObj= this.SelectProducts(productEntityObj.Product_Id);

            this.UpdateProductData(tblProductObj,productEntityObj);
            return "Updated Successfully";
        }
        #endregion

        #region Priate Method
        private Boolean UpdateProductData(tblProduct tblProductObj, ProductEntity productEntityObj)
        {

            tblProductObj.Product_Id = productEntityObj.Product_Id;
            tblProductObj.Product_Name = productEntityObj.Product_Name;
            tblProductObj.Product_Price = productEntityObj.Product_Price;
            tblProductObj.PRoduct_AvailableQuantity = productEntityObj.PRoduct_AvailableQuantity;

            dc.SubmitChanges();
            return true;

        }


        #endregion

        #endregion

        #region Select

        #region Public Method
        public void SelectData(ProductEntity productEntityObj)
        {
          tblProductObj= SelectProducts(productEntityObj.Product_Id);
          // System.Console.WriteLine($"Product Id:{tblProductObj.Product_Id}\nProduct Name:{tblProductObj.Product_Name}\nProduct Price:{tblProductObj.Product_Price}\nQuantity Available:{tblProductObj.PRoduct_AvailableQuantity}");
        }

       
        #endregion

        #region Private Method

        private tblProduct SelectProducts(int id)
        {
            return
               dc
               .tblProducts
              .AsEnumerable()
                  .FirstOrDefault((leLinqtblProductObj) => leLinqtblProductObj.Product_Id == Convert.ToDecimal(id));

        }
        #endregion
        #endregion

    }
}
