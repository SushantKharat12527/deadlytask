﻿CREATE TABLE [dbo].[tblPersonLogin] (
    [Person_Id]   INT            NOT NULL,
    [UserName]    NVARCHAR (MAX) NOT NULL,
    [Password]    NVARCHAR (MAX) NOT NULL,
    [Person_Type] NVARCHAR (MAX) NOT NULL,
    FOREIGN KEY ([Person_Id]) REFERENCES [dbo].[tblPerson] ([Person_Id])
);

