﻿CREATE TABLE [dbo].[tblProducts] (
    [Product_Id]                INT            NOT NULL,
    [Product_Name]              NVARCHAR (MAX) NOT NULL,
    [Product_Price]             MONEY          NOT NULL,
    [PRoduct_AvailableQuantity] NUMERIC (18)   NOT NULL,
    CONSTRAINT [PK_tblProducts] PRIMARY KEY CLUSTERED ([Product_Id] ASC)
);

