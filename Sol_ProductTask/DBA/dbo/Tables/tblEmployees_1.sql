﻿CREATE TABLE [dbo].[tblEmployees] (
    [Person_Id]   INT NOT NULL,
    [Employee_Id] INT IDENTITY (1, 1) NOT NULL,
    CONSTRAINT [FK__tblEmploy__Perso__1A14E395] FOREIGN KEY ([Person_Id]) REFERENCES [dbo].[tblPerson] ([Person_Id])
);

