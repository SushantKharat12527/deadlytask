﻿CREATE TABLE [dbo].[tblPerson] (
    [Person_Id]   INT            NOT NULL,
    [FirstName]   NVARCHAR (MAX) NOT NULL,
    [LastName]    NVARCHAR (MAX) NOT NULL,
    [Person_Type] NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_tblPerson] PRIMARY KEY CLUSTERED ([Person_Id] ASC)
);

