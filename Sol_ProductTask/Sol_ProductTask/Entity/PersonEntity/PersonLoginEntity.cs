﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_ProductTask.Entity.PersonEntity
{
   public class PersonLoginEntity
    {
        public string UserName { get; set; }

        public string Password { get; set; }
    }
}
