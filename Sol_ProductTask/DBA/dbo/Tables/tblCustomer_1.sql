﻿CREATE TABLE [dbo].[tblCustomer] (
    [Person_Id]   INT NOT NULL,
    [Customer_Id] INT IDENTITY (1, 1) NOT NULL,
    CONSTRAINT [FK__tblCustom__Perso__1BFD2C07] FOREIGN KEY ([Person_Id]) REFERENCES [dbo].[tblPerson] ([Person_Id])
);

