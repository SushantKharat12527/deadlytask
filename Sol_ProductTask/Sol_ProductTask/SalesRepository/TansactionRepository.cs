﻿using Sol_ProductTask.Admin;
using Sol_ProductTask.Entity;
using Sol_ProductTask.Entity.SalesEntity;
using Sol_ProductTask.ORD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_ProductTask.SalesRepository
{
   public class TansactionRepository
    {
        #region Declaration
  
        #endregion

        #region Constructor
        public TansactionRepository()
        {

            
        }
        #endregion
        #region Transaction
        AdminRepository adminRepositoryObj = new AdminRepository();
        #region Public Method
        public string InsertTransaction(tblProduct tblProductObj, SalesTransactionEntity salesTransactionEntityObj)
        {
            int Identity = this.AddSalesInfo(tblProductObj, salesTransactionEntityObj);
            return "Inserted Successfully";
        }
        #endregion

        #region Private Method
        private int AddSalesInfo(tblProduct tblProductObj,SalesTransactionEntity salesTransactionEntityObj)
        {
            decimal Total_Amount = this.GetTotalPrice(tblProductObj,salesTransactionEntityObj);
            tblSalesTransaction tblSalesTransactionObj = new tblSalesTransaction()
            {
               SalesDate = DateTime.Now,
               ProductId=adminRepositoryObj.tblProductObj.Product_Id,
               TotalQuantity=salesTransactionEntityObj.OrderQuantity,
               TotalAmount=Total_Amount,
               Status="Delivered"
               
            };
            adminRepositoryObj.
            dc.tblSalesTransactions
            
            .InsertOnSubmit(tblSalesTransactionObj);

            adminRepositoryObj.
           dc.SubmitChanges();

            var getLastIdentityValue = salesTransactionEntityObj.ProductId;
            return Convert.ToInt32(getLastIdentityValue);

        }
        #endregion
        #endregion


        #region Private Method
        private decimal GetTotalPrice(tblProduct tblProductObj, SalesTransactionEntity SalesEntityObj)
        {
            
            return tblProductObj.Product_Price * SalesEntityObj.OrderQuantity;

        }

        #endregion
    

    }
}
