﻿CREATE PROCEDURE [dbo].[uspSetUserLogin]
	@Command Varchar(50)=NULL,
	
	@PersonId Numeric(18,0)=NULL,
	@UserName Varchar(50)=NULL,
	@Password Varchar(50)=NULL

	

	 
AS
	BEGIN 
		
		DECLARE @ErrorMessage varchar(MAX)

			IF @Command='Insert'
				BEGIN
					
					BEGIN TRANSACTION

					BEGIN TRY 
						
						INSERT INTO tblPersonLogin
						(
							Person_Id,
							UserName,
							Password
						)
						VALUES
						(
							@PersonId,
							@UserName,
							@Password
						)

						

						COMMIT TRANSACTION
					END TRY 

					BEGIN CATCH 
						SET @ErrorMessage=ERROR_MESSAGE()
						ROLLBACK TRANSACTION
						
						

						RAISERROR(@ErrorMessage,16,1)
					END CATCH

				END
			ELSE IF @Command='Update'
				BEGIN 
					
					BEGIN TRANSACTION
						
						BEGIN TRY 
							
							SELECT 
						
						@UserName=CASE WHEN @UserName IS NULL THEN UL.UserName ELSE @UserName END,
						@Password=CASE WHEN @Password IS NULL THEN UL.Password ELSE @Password END

							FROM tblPersonLogin AS UL
								WHERE UL.Person_Id=@PersonId

								UPDATE tblPersonLogin
									SET 
										UserName=@UserName,
										Password=@Password
											WHERE Person_Id=@PersonId

											COMMIT TRANSACTION
						END TRY 

						BEGIN CATCH 
							SET @ErrorMessage=ERROR_MESSAGE()
							ROLLBACK TRANSACTION
							RAISERROR(@ErrorMessage,16,1)
						END CATCH 


					
				END
			ELSE IF @Command='Delete'
				BEGIN
					BEGIN TRANSACTION
						
						BEGIN TRY 

							DELETE FROM tblPersonLogin 
								WHERE Person_Id=@PersonId

											COMMIT TRANSACTION
						END TRY 

						BEGIN CATCH 
							SET @ErrorMessage=ERROR_MESSAGE()
							ROLLBACK TRANSACTION
							RAISERROR(@ErrorMessage,16,1)
						END CATCH 


				END

	END 
GO
	