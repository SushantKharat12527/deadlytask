﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_ProductTask.Entity
{
   public class ProductEntity
    {
        public int Product_Id { get; set; }

        public string Product_Name { get; set; }

        public decimal Product_Price { get; set; }

        public long PRoduct_AvailableQuantity { get; set; }
    }
}
