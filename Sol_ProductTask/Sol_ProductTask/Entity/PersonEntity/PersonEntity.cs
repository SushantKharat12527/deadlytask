﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_ProductTask.Entity.PersonEntity
{
   public class PersonEntity
    {
        public decimal? PersonId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserType { get; set; }

        public PersonLoginEntity LoginEntity { get; set; }

        public PersonCommunicationEntity CommunicationEntity { get; set; }

      
    }
}
