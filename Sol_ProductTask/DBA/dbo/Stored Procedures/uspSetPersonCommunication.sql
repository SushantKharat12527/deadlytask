﻿CREATE PROCEDURE [dbo].[uspSetUserCommunication]
	
	@Command Varchar(50)=NULL,
	
	@PersonId Numeric(18,0)=NULL,
	@MobileNo Varchar(50)=NULL,
	@EmailId Varchar(50)=NULL

AS
BEGIN 
		
		DECLARE @ErrorMessage varchar(MAX)

			IF @Command='Insert'
				BEGIN
					
					BEGIN TRANSACTION

					BEGIN TRY 
						
						INSERT INTO tblPersonCommunication
						(
							Person_Id,
						    Mobile_No,
							EmailId
						)
						VALUES
						(
							@PersonId,
							@MobileNo,
							@EmailId
						)

						
						COMMIT TRANSACTION
					END TRY 

					BEGIN CATCH 
						SET @ErrorMessage=ERROR_MESSAGE()
						ROLLBACK TRANSACTION

						RAISERROR(@ErrorMessage,16,1)
					END CATCH

				END
			ELSE IF @Command='Update'
				BEGIN 
					
					BEGIN TRANSACTION

					BEGIN TRY 
						
						SELECT 
						
						@MobileNo=CASE WHEN @MobileNo IS NULL THEN UC.Mobile_No ELSE @MobileNo END,
						@EmailId=CASE WHEN @EmailId IS NULL THEN UC.EmailId ELSE @EmailId END
						FROM tblPersonCommunication AS UC 
							WHERE UC.Person_Id=@PersonId

							UPDATE tblPersonCommunication
								SET 
									EmailId=@EmailId,
									Mobile_No=@MobileNo
										WHERE Person_Id=@PersonId

										COMMIT TRANSACTION

					END TRY 

					BEGIN CATCH 
					SET @ErrorMessage=ERROR_MESSAGE()
						ROLLBACK TRANSACTION

						RAISERROR(@ErrorMessage,16,1)

					END CATCH 

					

				END
			ELSE IF @Command='Delete'
				BEGIN
					BEGIN TRANSACTION

					BEGIN TRY 
						
						DELETE FROM tblPersonCommunication	
							WHERE Person_Id=@PersonId

										COMMIT TRANSACTION

					END TRY 

					BEGIN CATCH 
					SET @ErrorMessage=ERROR_MESSAGE()
						ROLLBACK TRANSACTION

						RAISERROR(@ErrorMessage,16,1)

					END CATCH 

					
				END

	END 
GO
